from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from django.conf.urls.static import static
from apps.agencia.views import AcaoListView, AcaoCreateView, AcaoUpdateView, AcaoDeleteView, \
    AgenciaListView, AgenciaCreateView, AgenciaUpdateView, AgenciaDeleteView
from apps.agencia.viewsets import AgenciaListViewSet
from apps.core.views import HomeView, LogoutView, LoginView
from apps.core.viewsets import EstadoListViewSet
from apps.job.views import JobListView, JobCreateView, JobUpdateView, JobDeleteView, TipoJobListView, TipoJobCreateView, \
    TipoJobUpdateView, TipoJobDeleteView, CandidaturaView, CandidaturaJogoView
from apps.job.viewsets import JobListViewSet, TipoJobViewSet, JobViewSet, CandidaturaViewSet, CandidaturaStatusViewSet
from apps.promotor.views import PromotorCredenciadoListView, PromotorCredenciadoUpdateView
from apps.promotor.viewsets import PromotorCreateViewSet, CorDaPeleListViewSet, CorDoCabeloListViewSet, \
    CorDosOlhosListViewSet, LoginPromotorViewSet, PromotorViewSet, PromotorGetViewSet, PromotorUpdateViewSet, \
    CoresViewSet, ImagemCreateViewSet

router = DefaultRouter()
router.register(r'tipo-job', TipoJobViewSet)
router.register(r'job', JobViewSet)
router.register(r'promotor', PromotorViewSet)
router.register(r'agencia-list', AgenciaListViewSet)
router.register(r'estado-list', EstadoListViewSet)
router.register(r'candidatura', CandidaturaViewSet)

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^api/imagem-save/', ImagemCreateViewSet.as_view()),
    url(r'^api/login-promotor/', LoginPromotorViewSet.as_view()),
    url(r'^api/job-list/', JobListViewSet.as_view()),
    url(r'^api/promotor/', PromotorGetViewSet.as_view()),
    url(r'^api/promotor-create/', PromotorCreateViewSet.as_view()),
    url(r'^api/promotor-update/(?P<pk>[0-9]+)/', PromotorUpdateViewSet.as_view()),
    url(r'^api/candidatura/(?P<candidatura>[0-9]+)/status/(?P<status>[0-9]+)/', CandidaturaStatusViewSet.as_view()),
    url(r'^api/cordapele-list/', CorDaPeleListViewSet.as_view()),
    url(r'^api/cordocabelo-list/', CorDoCabeloListViewSet.as_view()),
    url(r'^api/cordosolhos-list/', CorDosOlhosListViewSet.as_view()),
    url(r'^api/cores/', CoresViewSet.as_view()),

    url(regex='^login/$', view=LoginView.as_view(), name='login'),
    url(regex='^logout/$', view=LogoutView.as_view(), name='logout'),
    url(regex='^$', view=HomeView.as_view(), name='home'),

    url(regex='^cadastros/agencia/$', view=AgenciaListView.as_view(), name='cadastros-agencia-list'),
    url(regex='^cadastros/agencia/create$', view=AgenciaCreateView.as_view(), name='cadastros-agencia-create'),
    url(regex='^cadastros/agencia/(?P<pk>[0-9]+)/$', view=AgenciaUpdateView.as_view(), name='cadastros-agencia-update'),
    url(regex='^cadastros/agencia/(?P<pk>[0-9]+)/delete/$', view=AgenciaDeleteView.as_view(),
        name='cadastros-agencia-delete'),

    url(regex='^cadastros/acao/$', view=AcaoListView.as_view(), name='cadastros-acao-list'),
    url(regex='^cadastros/acao/create$', view=AcaoCreateView.as_view(), name='cadastros-acao-create'),
    url(regex='^cadastros/acao/(?P<pk>[0-9]+)/$', view=AcaoUpdateView.as_view(), name='cadastros-acao-update'),
    url(regex='^cadastros/acao/(?P<pk>[0-9]+)/delete/$', view=AcaoDeleteView.as_view(), name='cadastros-acao-delete'),

    url(regex='^cadastros/tipojob/$', view=TipoJobListView.as_view(), name='cadastros-tipojob-list'),
    url(regex='^cadastros/tipojob/create$', view=TipoJobCreateView.as_view(), name='cadastros-tipojob-create'),
    url(regex='^cadastros/tipojob/(?P<pk>[0-9]+)/$', view=TipoJobUpdateView.as_view(), name='cadastros-tipojob-update'),
    url(regex='^cadastros/tipojob/(?P<pk>[0-9]+)/delete/$', view=TipoJobDeleteView.as_view(),
        name='cadastros-tipojob-delete'),

    url(regex='^cadastros/job/$', view=JobListView.as_view(), name='cadastros-job-list'),
    url(regex='^cadastros/job/create$', view=JobCreateView.as_view(), name='cadastros-job-create'),
    url(regex='^cadastros/job/(?P<pk>[0-9]+)/$', view=JobUpdateView.as_view(), name='cadastros-job-update'),
    url(regex='^cadastros/job/(?P<pk>[0-9]+)/delete/$', view=JobDeleteView.as_view(), name='cadastros-job-delete'),

    url(regex='^cadastros/credenciamento/$', view=PromotorCredenciadoListView.as_view(),
        name='cadastros-credenciamento-list'),
    url(regex='^cadastros/credenciamento/(?P<pk>[0-9]+)/$', view=PromotorCredenciadoUpdateView.as_view(),
        name='cadastros-credenciamento-update'),

    url(regex='^candidatura/list/$', view=CandidaturaView.as_view(), name='candidatura-list'),
    url(regex='^candidatura/jogo/$', view=CandidaturaJogoView.as_view(), name='candidatura-jogo'),

    url(regex='^cadastros/credenciamento/$', view=PromotorCredenciadoListView.as_view(),
        name='cadastros-credenciamento-list'),
    url(regex='^cadastros/credenciamento/(?P<pk>[0-9]+)/$', view=PromotorCredenciadoUpdateView.as_view(),
        name='cadastros-credenciamento-update'),
]

if settings.DEBUG:
    urlpatterns += static('media/', document_root=settings.MEDIA_ROOT)
