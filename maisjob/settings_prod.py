from logging.handlers import SysLogHandler

from .settings import *

DEBUG = False

MEDIA_URL = 'https://maisjob.rnsoft.com.br/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

STATICFILES_DIRS = ()
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'maisjob',
        'USER': 'postgres',
        'PASSWORD': 'RND3vLtd@',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    },
}

LOG_FOLDER = os.path.join(BASE_DIR, 'logs')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'file': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': os.path.join(LOG_FOLDER, 'error.log'),
        },
    }, 'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'ERROR',
            'propagate': True,
        },
    },
}
