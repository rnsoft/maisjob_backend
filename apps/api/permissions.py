from django.contrib import messages
from django.contrib.auth.mixins import AccessMixin
from django.contrib.auth.models import AnonymousUser
from django.http import HttpResponseRedirect
from django.urls import reverse
from rest_framework.permissions import BasePermission

from apps.core.models import Usuario
from apps.promotor.models import Promotor
from rest_framework import filters


class IsAuthenticated(BasePermission):
    def has_permission(self, request, view):
        if isinstance(request.user, Usuario):
            return request.user
        else:
            return False


class IsPromotor(BasePermission):
    def has_permission(self, request, view):
        if not isinstance(request.user, AnonymousUser):
            promotor = Promotor.objects.filter(usuario=request.user)
            if promotor:
                if promotor[0].usuario.token == request.user.token:
                    return request.user

        return False


class IsAdmin(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_superuser


class IsPromotorBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        id = request.user.promotor.id
        return queryset.filter(id=id)


class IsCandidaturaPromotorBackend(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        id = request.user.promotor.id
        return queryset.filter(promotor_id=id)


class SuperUserRequiredMixin(AccessMixin):

    def get_url(self):
        messages.error(self.request, 'Não autorizado.')
        return reverse('home')

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            return HttpResponseRedirect(self.get_url())
        return super(SuperUserRequiredMixin, self).dispatch(request, *args, **kwargs)
