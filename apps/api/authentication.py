from rest_framework import authentication
from rest_framework.authentication import get_authorization_header
from rest_framework import exceptions

from apps.core.models import Usuario


class UsuarioAuthentication(authentication.BaseAuthentication):
    def authenticate(self, request):
        auth = get_authorization_header(request).split()
        if not auth or auth[0].lower() != b'token':
            return None

        if len(auth) == 1:
            msg = 'Invalid key header. No credentials provided.'
            raise exceptions.AuthenticationFailed(msg)
        elif len(auth) > 2:
            msg = 'Invalid key header. key string should not contain spaces.'
            raise exceptions.AuthenticationFailed(msg)

        try:
            key = auth[1].decode()
        except UnicodeError:
            msg = 'Invalid key header. key string should not contain invalid characters.'
            raise exceptions.AuthenticationFailed(msg)
        return self.authenticate_credentials(key)

    def authenticate_credentials(self, key):
        try:
            usuario = Usuario.objects.get(token=key)

        except Usuario.DoesNotExist:
            raise exceptions.AuthenticationFailed('Falha de autenticação')
        return usuario, usuario.token

    def authenticate_header(self, request):
        return 'key'
