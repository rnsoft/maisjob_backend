from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404

# Create your views here.
from django.urls import reverse
from django.views import View
from django.views.generic import ListView, CreateView, UpdateView

from apps.api.permissions import SuperUserRequiredMixin
from apps.promotor.forms import PromotorCredenciadoForm
from apps.promotor.models import PromotorCredenciado


class PromotorCredenciadoListView(SuperUserRequiredMixin, LoginRequiredMixin, ListView):
    model = PromotorCredenciado
    template_name = 'cadastros/credenciamento/lista.html'
    context_object_name = "credenciamento_lista"


class PromotorCredenciadoUpdateView(SuperUserRequiredMixin, LoginRequiredMixin, UpdateView):
    model = PromotorCredenciado
    template_name = 'cadastros/credenciamento/form.html'
    form_class = PromotorCredenciadoForm

    def get_success_url(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-credenciamento-list')

    def get_success_url_novo(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-credenciamento-create')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        self.object.promotor.credenciar()
        if 'outro' in self.request.POST:
            return HttpResponseRedirect(self.get_success_url_novo())
        else:
            return HttpResponseRedirect(self.get_success_url())
