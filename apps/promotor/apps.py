from django.apps import AppConfig


class PromotorConfig(AppConfig):
    name = 'apps.promotor'
