import base64
import datetime
import re
from unicodedata import normalize
import os

from django.conf import settings
from django.core.files.base import ContentFile
from django.db import transaction
from rest_framework import serializers

from apps.agencia.models import Agencia
from apps.core.models import Usuario
from apps.core.serializers import UsuarioSerializer, UsuarioUpdateSerializer, CidadeSerializer
from .models import Promotor, CorDaPele, CorDoCabelo, CorDosOlhos, Imagem, PromotorCredenciado


class LoginPromotorSerializer(serializers.Serializer):
    email = serializers.EmailField()
    senha = serializers.CharField()

    def validate(self, attrs):
        try:
            promotor = Promotor.objects.get(usuario__email=attrs.get('email'))
            if not promotor.usuario.check_password(attrs.get('senha')):
                raise serializers.ValidationError({"Algo deu errado": ['E-mail ou senha inválidos.']},
                                                  code='authorization')
        except Promotor.DoesNotExist:
            raise serializers.ValidationError({"Algo deu errado": ['E-mail ou senha inválidos.']}, code='authorization')

        return attrs


class ImagemSerializer(serializers.ModelSerializer):
    path = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Imagem
        fields = ('id', 'path', 'acao', 'promotor', 'tipo',)

    def get_path(self, instance):
        return instance.path.url


class PromotorSerializer(serializers.ModelSerializer):
    usuario = UsuarioSerializer(read_only=True)
    imagens = serializers.SerializerMethodField(read_only=True)
    sexo__display = serializers.SerializerMethodField(read_only=True)
    cidade = CidadeSerializer(read_only=True)

    class Meta:
        model = Promotor
        fields = ('id', 'usuario', 'nome_exibicao', 'nome_completo', 'cpf', 'data_nascimento', 'sexo', 'peso', 'altura',
                  'cor_da_pele', 'cor_do_cabelo', 'cor_dos_olhos', 'telefone', 'sexo__display', 'imagens', 'cidade', 'codigoindicacao')

    def get_sexo__display(self, instance):
        return instance.get_sexo_display()

    def get_imagens(self, instance):
        return ImagemSerializer(instance.imagens.all(), many=True).data


class PromotorUpdateSerializer(serializers.ModelSerializer):
    usuario = UsuarioUpdateSerializer(validators=[])
    imagens = serializers.ListField(allow_empty=True)

    class Meta:
        model = Promotor
        fields = ('id', 'usuario', 'nome_exibicao', 'nome_completo', 'cpf', 'data_nascimento', 'sexo', 'peso', 'altura',
                  'cor_da_pele', 'cor_do_cabelo', 'cor_dos_olhos', 'telefone', 'imagens', 'cidade')

    def update(self, instance, validated_data):
        with transaction.atomic():
            usuario_data = validated_data.get('usuario')

            if usuario_data.get('senha'):
                senha = Usuario()
                senha.set_password(usuario_data['senha'])
            else:
                senha = None

            usuario = self.instance.usuario

            if not self.instance.usuario.email == usuario_data['email']:
                if senha:
                    usuario.email = usuario_data['email']
                    usuario.password = senha.password
                    usuario.save()
                else:
                    usuario.email = usuario_data['email']
                    usuario.save()
            else:
                if senha:
                    usuario.email = usuario_data['email']
                    usuario.password = senha.password
                    usuario.save()

            promotor = self.instance

            promotor.nome_completo = validated_data.get('nome_completo', promotor.nome_completo)
            promotor.cor_dos_olhos = validated_data.get('cor_dos_olhos', promotor.cor_dos_olhos)
            promotor.cpf = validated_data.get('cpf', promotor.cpf)
            promotor.cor_do_cabelo = validated_data.get('cor_do_cabelo', promotor.cor_do_cabelo)
            promotor.nome_exibicao = validated_data.get('nome_exibicao', promotor.nome_exibicao)
            promotor.telefone = validated_data.get('telefone', promotor.telefone)
            promotor.altura = validated_data.get('altura', promotor.altura)
            promotor.sexo = validated_data.get('sexo', promotor.sexo)
            promotor.cor_da_pele = validated_data.get('cor_da_pele', promotor.cor_da_pele)
            promotor.data_nascimento = validated_data.get('data_nascimento', promotor.data_nascimento)
            promotor.peso = validated_data.get('peso', promotor.peso)
            promotor.cidade = validated_data.get('cidade', promotor.cidade)

            promotor.save()

            for image in validated_data.get('imagens'):
                imagens_salvas = Imagem.objects.filter(promotor=promotor, tipo=image['tipo'])
                for i in imagens_salvas:
                    i.deletar_arquivo()

                imagem = Imagem.objects.create(promotor=promotor, path='', tipo=image['tipo'])
                imagem.salvar64(image['imagem'])

            return promotor

    def validar_cpf(self, cpf, d1=0, d2=0, i=0):
        while i < 10:
            d1, d2, i = (d1 + (int(cpf[i]) * (11 - i - 1))) % 11 if i < 9 else d1, (
                d2 + (int(cpf[i]) * (11 - i))) % 11, i + 1
        return (int(cpf[9]) == (11 - d1 if d1 > 1 else 0)) and (int(cpf[10]) == (11 - d2 if d2 > 1 else 0))

    def validate(self, data):
        usuario = self.context['request'].data.get('usuario')

        if usuario.get('nova_senha') and not usuario.get('senha'):
            raise serializers.ValidationError('Senha atual não informada.')

        if usuario.get('nova_senha') and usuario.get('senha'):
            if self.instance.usuario.check_password(usuario.get('senha')):
                data['usuario']['senha'] = usuario.get('nova_senha')
            else:
                raise serializers.ValidationError('Senha atual incorreta.')

        if not self.instance.usuario.email == data.get('usuario')['email']:
            if Promotor.objects.filter(usuario__email=data.get('usuario')['email']):
                raise serializers.ValidationError('E-mail já cadastrado.')

        if len(data.get('cpf')) < 11:
            raise serializers.ValidationError('CPF inválido.')

        if not self.validar_cpf(data.get('cpf')):
            raise serializers.ValidationError('CPF inválido.')

        return data


class PromotorCreateSerializer(serializers.ModelSerializer):
    usuario = UsuarioSerializer()

    class Meta:
        model = Promotor
        fields = ('id', 'usuario', 'nome_exibicao', 'nome_completo', 'cpf', 'data_nascimento', 'sexo', 'peso', 'altura',
                  'cor_da_pele', 'cor_do_cabelo', 'cor_dos_olhos', 'telefone', 'cidade')

    def create(self, validated_data):
        with transaction.atomic():
            usuario_data = validated_data.get('usuario')
            senha = Usuario()
            senha.set_password(usuario_data['senha'])
            usuario = Usuario.objects.create(email=usuario_data['email'], password=senha.password)
            promotor = Promotor.objects.create(
                nome_completo=validated_data['nome_completo'],
                cor_dos_olhos=validated_data['cor_dos_olhos'],
                cpf=validated_data['cpf'],
                cor_do_cabelo=validated_data['cor_do_cabelo'],
                nome_exibicao=validated_data['nome_exibicao'],
                telefone=validated_data['telefone'],
                altura=validated_data['altura'],
                sexo=validated_data['sexo'],
                cor_da_pele=validated_data['cor_da_pele'],
                data_nascimento=validated_data['data_nascimento'],
                peso=validated_data['peso'],
                cidade=validated_data['cidade'],
                usuario=usuario
            )

            codigo_indicacao = self.context['request'].data.get('codigo_indicacao')

            agencia = Agencia.objects.filter(codigo_indicacao=codigo_indicacao)

            if agencia:
                PromotorCredenciado.objects.create(
                    promotor=promotor,
                    agencia_indicacao=agencia[0]
                )
            else:
                promo_incacao = Promotor.objects.filter(codigoindicacao=codigo_indicacao)
                if promo_incacao:
                    PromotorCredenciado.objects.create(
                        promotor=promotor,
                        promotor_indicacao=promo_incacao[0]
                    )
                else:
                    PromotorCredenciado.objects.create(
                        promotor=promotor
                    )

            return promotor

    def validar_cpf(self, cpf, d1=0, d2=0, i=0):
        while i < 10:
            d1, d2, i = (d1 + (int(cpf[i]) * (11 - i - 1))) % 11 if i < 9 else d1, (
                d2 + (int(cpf[i]) * (11 - i))) % 11, i + 1
        return (int(cpf[9]) == (11 - d1 if d1 > 1 else 0)) and (int(cpf[10]) == (11 - d2 if d2 > 1 else 0))

    def validate(self, data):
        usuario = self.context['request'].data.get('usuario')

        if not usuario.get('senha'):
            raise serializers.ValidationError('Senha não informada.')

        if len(data.get('cpf')) < 11:
            raise serializers.ValidationError('CPF inválido.')

        if not self.validar_cpf(data.get('cpf')):
            raise serializers.ValidationError('CPF inválido.')

        codigo_indicacao = self.context['request'].data.get('codigo_indicacao')

        if codigo_indicacao:
            agencia = Agencia.objects.filter(codigo_indicacao=codigo_indicacao)

            if not agencia:
                promo_incacao = Promotor.objects.filter(codigoindicacao=codigo_indicacao)
                if not promo_incacao:
                    raise serializers.ValidationError('Código indicação inválido.')

        data['usuario']['senha'] = usuario.get('senha')

        return data


class CorDaPeleSerializer(serializers.ModelSerializer):
    class Meta:
        model = CorDaPele
        fields = ('id', 'descricao',)


class CorDoCabeloSerializer(serializers.ModelSerializer):
    class Meta:
        model = CorDoCabelo
        fields = ('id', 'descricao',)


class CorDosOlhosSerializer(serializers.ModelSerializer):
    class Meta:
        model = CorDosOlhos
        fields = ('id', 'descricao',)


class ImagemSaveSerializer(serializers.ModelSerializer):
    url = serializers.SerializerMethodField(read_only=True)
    path = serializers.CharField()
    promotor = serializers.PrimaryKeyRelatedField(queryset=Promotor.objects.all())

    class Meta:
        model = Imagem
        fields = ('id', 'path', 'tipo', 'promotor', 'acao', 'url')

    def get_url(self, instance):
        return instance.path.url

    def create(self, validated_data):
        promotor = validated_data['promotor']

        imagens_salvas = Imagem.objects.filter(promotor=promotor, tipo=validated_data['tipo'])

        for i in imagens_salvas:
            i.deletar_arquivo()

        image = Imagem.objects.create(promotor=promotor, path='', tipo=validated_data['tipo'])
        image.salvar64(validated_data['path'])
        return image
