from django import forms

from apps.agencia.models import Agencia
from .models import PromotorCredenciado, Promotor


class PromotorCredenciadoForm(forms.ModelForm):
    class Meta:
        model = PromotorCredenciado
        fields = ('data_pagamento', 'valor_pago',)
