import base64
from datetime import date

import os
from django.conf import settings
from django.core.files.base import ContentFile
from django.db import models
from unicodedata import normalize

# Create your models here.
from simple_history.models import HistoricalRecords
from hashids import Hashids
from random import randint


class Promotor(models.Model):
    PREFIRO_NAO_DIZER = 1
    MASCULINO = 2
    FEMININO = 3

    SEXO = (
        (PREFIRO_NAO_DIZER, 'Prefiro não dizer'),
        (MASCULINO, 'Masculino'),
        (FEMININO, 'Feminino'),
    )

    CADASTRADO = 0
    CREDENCIADO = 1

    STATUS = (
        (CADASTRADO, 'Cadastrado'),
        (CREDENCIADO, 'Credenciado'),
    )

    codigoindicacao = models.CharField(max_length=8, unique=True, null=True)
    status = models.PositiveIntegerField(choices=STATUS, default=0)
    usuario = models.OneToOneField(to='core.Usuario', related_name='promotor', on_delete=models.PROTECT)
    nome_exibicao = models.CharField(max_length=255, )
    nome_completo = models.CharField(max_length=255, )
    cpf = models.CharField(max_length=15)
    data_nascimento = models.DateField()
    sexo = models.PositiveIntegerField(choices=SEXO)
    peso = models.FloatField()
    altura = models.IntegerField()
    cor_da_pele = models.ForeignKey(to='CorDaPele', on_delete=models.PROTECT, related_name='promotores')
    cor_do_cabelo = models.ForeignKey(to='CorDoCabelo', on_delete=models.PROTECT, related_name='promotores')
    cor_dos_olhos = models.ForeignKey(to='CorDosOlhos', on_delete=models.PROTECT, related_name='promotores')
    cidade = models.ForeignKey(to='core.Cidade', on_delete=models.PROTECT, related_name='promotores')
    telefone = models.CharField(max_length=255)

    def __str__(self):
        return self.nome_exibicao

    def idade(self):
        today = date.today()
        data_nascimento = self.data_nascimento
        idade = today.year - data_nascimento.year - (
            (today.month, today.day) < (data_nascimento.month, data_nascimento.day))
        return idade

    def cpf_mascara(self):
        cpf = self.cpf
        return '{}.{}.{}-{}'.format(cpf[:3], cpf[3:6], cpf[6:9], cpf[9:])

    def credenciar(self, *args, **kwargs):
        try:
            new_numero = Promotor.objects.latest('id').id + 1
        except Promotor.DoesNotExist:
            new_numero = 1
        new_numero = new_numero + randint(0, 9999)
        hash = Hashids(salt='novo_promotor', min_length=8).encrypt(new_numero)

        self.codigoindicacao = hash.lower()
        self.save()
        return True

    def job_para_min(self, job):
        cores_dos_olhos = [o.cor_dos_olhos_id for o in job.cores_dos_olhos.all()]
        cores_da_peles = [o.cor_da_pele_id for o in job.cores_da_peles.all()]
        cores_dos_cabelos = [o.cor_do_cabelo_id for o in job.cores_dos_cabelos.all()]
        arrayValidacao = [True, True, True, True, True, True, True, True]

        if job.perfil_sexo:
            if not job.perfil_sexo == 1:
                if not self.sexo == job.perfil_sexo:
                    arrayValidacao[0] = False

        if cores_dos_olhos:
            if not self.cor_dos_olhos_id in cores_dos_olhos:
                arrayValidacao[1] = False

        if cores_da_peles:
            if not self.cor_da_pele_id in cores_da_peles:
                arrayValidacao[2] = False

        if cores_dos_cabelos:
            if not self.cor_do_cabelo_id in cores_dos_cabelos:
                arrayValidacao[3] = False

        if job.perfil_altura_minima:
            if not self.altura >= job.perfil_altura_minima:
                arrayValidacao[4] = False

        if job.perfil_altura_maxima:
            if not self.altura <= job.perfil_altura_maxima:
                arrayValidacao[5] = False

        if job.perfil_idade_minima:
            if not self.idade() >= job.perfil_idade_minima:
                arrayValidacao[6] = False

        if job.perfil_idade_maxima:
            if not self.idade() <= job.perfil_idade_maxima:
                arrayValidacao[7] = False

        for validacao in arrayValidacao:
            if validacao == False:
                return False

        return True

    history = HistoricalRecords(table_name='promotor_promotor_history')


class PromotorCredenciado(models.Model):
    promotor = models.OneToOneField(to='Promotor', related_name='credenciamento', on_delete=models.PROTECT)
    data_pagamento = models.DateField(null=True)
    valor_pago = models.FloatField(null=True)
    promotor_indicacao = models.ForeignKey(to='Promotor', related_name='credenciamentos_indicados',
                                           on_delete=models.PROTECT, null=True)
    agencia_indicacao = models.ForeignKey(to='agencia.Agencia', related_name='credenciamentos_indicados',
                                          on_delete=models.PROTECT, null=True)

    history = HistoricalRecords(table_name='promotor_promotorcredenciado_history')

    def valor_pago_formatado(self):
        return "{0:.2f}".format(self.valor_pago)


class CorDaPele(models.Model):
    descricao = models.CharField(max_length=255)

    history = HistoricalRecords(table_name='promotor_cordapele_history')

    def __str__(self):
        return self.descricao


class CorDoCabelo(models.Model):
    descricao = models.CharField(max_length=255)

    history = HistoricalRecords(table_name='promotor_cordocabelo_history')

    def __str__(self):
        return self.descricao


class CorDosOlhos(models.Model):
    descricao = models.CharField(max_length=255)

    history = HistoricalRecords(table_name='promotor_cordosolhos_history')

    def __str__(self):
        return self.descricao


class Imagem(models.Model):
    FRENTE = 1
    PERFIL = 2
    COSTAS = 3
    OUTRAS = 4

    TIPO = (
        (FRENTE, 'Frente'),
        (PERFIL, 'Perfil'),
        (COSTAS, 'Costas'),
        (OUTRAS, 'Outros'),
    )

    path = models.ImageField(upload_to='imagem')
    tipo = models.PositiveIntegerField(choices=TIPO)
    promotor = models.ForeignKey(to='Promotor', on_delete=models.PROTECT, related_name='imagens', null=True)
    acao = models.ForeignKey(to='agencia.Acao', on_delete=models.PROTECT, related_name='imagens', null=True)

    history = HistoricalRecords(table_name='promotor_imagem_history')

    def deletar_arquivo(self):
        if self.id:
            try:
                nome = str(self.path).replace('imagem/', '')
                nome = nome.replace('imagem\\', '')
                local = os.path.join(settings.BASE_DIR, 'media', 'imagem', nome)
                os.remove(local)
            except OSError:
                pass

            self.delete()

            return True
        return False

    def salvar64(self, path64):
        if self.id:
            try:
                nome = normalize('NFKD', self.promotor.nome_exibicao).encode('ASCII', 'ignore').decode('ASCII')
            except:
                nome = 'erro_no_nome'

            format, imgstr = path64.split(';base64,')
            ext = format.split('/')[-1]
            data = ContentFile(base64.b64decode(imgstr))
            file_name = nome.replace(" ", "_") + "." + ext

            self.path.save(str(self.id) + '_' + file_name, data, save=True)
            return True
        return False
