from rest_framework import viewsets, status, generics
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from apps.api.permissions import IsPromotor, IsPromotorBackend
from .models import Promotor, CorDaPele, CorDoCabelo, CorDosOlhos, Imagem
from .serializers import PromotorCreateSerializer, CorDaPeleSerializer, CorDosOlhosSerializer, \
    CorDoCabeloSerializer, PromotorSerializer, LoginPromotorSerializer, PromotorUpdateSerializer, ImagemSaveSerializer


class LoginPromotorViewSet(GenericAPIView):
    permission_classes = ()
    serializer_class = LoginPromotorSerializer

    def post(self, request, *args, **kwargs):
        serializer = LoginPromotorSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            promotor = Promotor.objects.get(usuario__email=serializer.validated_data.get('email'))
            return Response(PromotorSerializer(promotor).data)

        return Response({'erro': 'Não permitido.'}, status=status.HTTP_400_BAD_REQUEST)


class PromotorGetViewSet(generics.ListAPIView):
    permission_classes = (IsPromotor)

    def get(self, request, *args, **kwargs):
        return Response(PromotorSerializer(Promotor.objects.get(usuario__email=request.user.email)).data)


class PromotorCreateViewSet(generics.CreateAPIView):
    queryset = Promotor.objects.all()
    serializer_class = PromotorCreateSerializer
    permission_classes = ()


class PromotorUpdateViewSet(generics.UpdateAPIView):
    queryset = Promotor.objects.all()
    serializer_class = PromotorUpdateSerializer
    permission_classes = ()

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=False)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        promotor = PromotorSerializer(serializer.instance)
        return Response(promotor.data, status=status.HTTP_201_CREATED)


class PromotorViewSet(viewsets.ModelViewSet):
    queryset = Promotor.objects.all()
    serializer_class = PromotorSerializer
    filter_backends = (IsPromotorBackend,)
    permission_classes = (IsPromotor,)


class CorDaPeleListViewSet(generics.ListAPIView):
    queryset = CorDaPele.objects.all()
    serializer_class = CorDaPeleSerializer
    permission_classes = ()


class CorDoCabeloListViewSet(generics.ListAPIView):
    queryset = CorDoCabelo.objects.all()
    serializer_class = CorDoCabeloSerializer
    permission_classes = ()


class CorDosOlhosListViewSet(generics.ListAPIView):
    queryset = CorDosOlhos.objects.all()
    serializer_class = CorDosOlhosSerializer
    permission_classes = ()


class ImagemCreateViewSet(generics.CreateAPIView):
    queryset = Imagem.objects.all()
    serializer_class = ImagemSaveSerializer
    permission_classes = ()


class ImagemViewSet(generics.ListAPIView):
    queryset = CorDosOlhos.objects.all()
    serializer_class = CorDosOlhosSerializer
    permission_classes = ()


class CoresViewSet(GenericAPIView):
    permission_classes = ()

    def get(self, request, *args, **kwargs):
        pele = CorDaPeleSerializer(CorDaPele.objects.all(), many=True).data
        cabelo = CorDoCabeloSerializer(CorDoCabelo.objects.all(), many=True).data
        olho = CorDosOlhosSerializer(CorDosOlhos.objects.all(), many=True).data

        return Response({"cordapele": pele, "cordocabelo": cabelo, "cordosolhos": olho})
