import binascii, os
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, AbstractUser
from django.contrib.auth.base_user import BaseUserManager


# Create your models here.
def generate_token():
    return binascii.hexlify(os.urandom(20)).decode()


class UsuarioManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)


class Usuario(AbstractBaseUser, PermissionsMixin):
    nome = models.CharField(max_length=150, null=True)
    email = models.EmailField('E-mail', unique=True)
    token = models.CharField(max_length=50, unique=True, blank=True)
    is_staff = models.BooleanField(default=False)
    datahora_cadastro = models.DateTimeField(auto_now_add=True)
    datahora_alteracao = models.DateTimeField(auto_now=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UsuarioManager()

    def save(self, *args, **kwargs):
        if not self.token:
            self.token = generate_token()
        return super(Usuario, self).save(*args, **kwargs)

    def get_short_name(self):
        return self.email


class Estado(models.Model):
    nome = models.CharField(max_length=150)
    sigla = models.CharField(max_length=2)

    class Meta():
        ordering = ['nome']


class Cidade(models.Model):
    nome = models.CharField(max_length=255)
    capital = models.BooleanField(default=False)
    estado = models.ForeignKey(to='Estado', on_delete=models.PROTECT, related_name='cidades')

    class Meta():
        ordering = ['-capital', 'nome']

    def __str__(self):
        return self.nome
