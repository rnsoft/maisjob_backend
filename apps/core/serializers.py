from rest_framework import serializers
from .models import Usuario, Estado, Cidade


class CidadeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cidade
        fields = ('id', 'nome', 'capital', 'estado',)


class EstadoSerializer(serializers.ModelSerializer):
    cidades = CidadeSerializer(read_only=True, many=True)

    class Meta:
        model = Estado
        fields = ('id', 'nome', 'sigla', 'cidades',)


class UsuarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Usuario
        fields = ('id', 'nome', 'email', 'token', 'datahora_cadastro', 'datahora_alteracao',)


class UsuarioUpdateSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(validators=[])

    class Meta:
        model = Usuario
        fields = ('id', 'nome', 'email', 'token', 'datahora_cadastro', 'datahora_alteracao',)
