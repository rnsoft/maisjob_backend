from rest_framework import viewsets
from .models import Estado
from .serializers import EstadoSerializer


class EstadoListViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Estado.objects.all()
    serializer_class = EstadoSerializer
    permission_classes = ()
