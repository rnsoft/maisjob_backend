from django.contrib.auth.forms import AuthenticationForm
from django import forms

from apps.core.models import Usuario


class AuthForm(AuthenticationForm):
    username = forms.EmailField(label="E-mail")
    password = forms.CharField(label="Senha", strip=False, widget=forms.PasswordInput)

    def clean_username(self):
        data = self.data
        usuario = Usuario.objects.filter(email=data['username'])

        if usuario:
            usuario = usuario[0]

            if not usuario.is_staff:
                raise forms.ValidationError('Não autorizado.')

        return data['username']
