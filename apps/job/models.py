from django.db import models

# Create your models here.
from simple_history.models import HistoricalRecords


class TipoJob(models.Model):
    HORA = 1
    DIARIA = 2
    SEMANA = 3
    MES = 4

    MODOS_PAGAMENTOS = (
        (HORA, 'Pagamento por hora'),
        (DIARIA, 'Pagamento por dia'),
        (SEMANA, 'Pagamento por semana'),
        (MES, 'Pagamento por mês')
    )

    descricao = models.CharField(max_length=255)
    modo_pagamento_sugerido = models.PositiveIntegerField(choices=MODOS_PAGAMENTOS)
    history = HistoricalRecords(table_name='job_tipojob_history')

    def __str__(self):
        return self.descricao


class Job(models.Model):
    PREFIRO_NAO_DIZER = 1
    MASCULINO = 2
    FEMININO = 3

    SEXO = (
        (PREFIRO_NAO_DIZER, 'Prefiro não dizer'),
        (MASCULINO, 'Masculino'),
        (FEMININO, 'Feminino'),
    )

    acao = models.ForeignKey(to='agencia.Acao', on_delete=models.PROTECT, related_name='jobs')
    tipo = models.ForeignKey(to='TipoJob', on_delete=models.PROTECT, related_name='jobs')
    modo_pagamento = models.PositiveIntegerField(choices=TipoJob.MODOS_PAGAMENTOS)
    quantidade_vagas_totais = models.IntegerField()
    quantidade_vagas_disponiveis = models.IntegerField()
    quantidade_de_modo_pagamento = models.IntegerField()
    cache_por_modo_pagamento = models.FloatField()
    cache_individual = models.FloatField()
    cache_total = models.FloatField()
    observacao = models.CharField(max_length=255, null=True)

    perfil_sexo = models.PositiveIntegerField(choices=SEXO, null=True)
    perfil_idade_minima = models.IntegerField(null=True)
    perfil_idade_maxima = models.IntegerField(null=True)
    perfil_altura_minima = models.FloatField(null=True)
    perfil_altura_maxima = models.FloatField(null=True)

    history = HistoricalRecords(table_name='job_job_history')

    def subtrair_disponnivel(self):
        job = Job.objects.filter(id=self.id)
        job.update(quantidade_vagas_disponiveis=job[0].quantidade_vagas_disponiveis - 1)

    def adicionar_disponnivel(self):
        job = Job.objects.filter(id=self.id)
        job.update(quantidade_vagas_disponiveis=job[0].quantidade_vagas_disponiveis + 1)

    def __str__(self):
        return str(self.acao)


class CorDaPeleJob(models.Model):
    job = models.ForeignKey(to='Job', on_delete=models.PROTECT, related_name='cores_da_peles')
    cor_da_pele = models.ForeignKey(to='promotor.CorDaPele', on_delete=models.PROTECT,
                                    related_name='cores_da_peles')
    history = HistoricalRecords(table_name='job_cordapelejob_history')


class CorDoCabeloJob(models.Model):
    job = models.ForeignKey(to='Job', on_delete=models.PROTECT, related_name='cores_dos_cabelos')
    cor_do_cabelo = models.ForeignKey(to='promotor.CorDoCabelo', on_delete=models.PROTECT,
                                      related_name='cores_dos_cabelos')
    history = HistoricalRecords(table_name='job_cordocabelojob_history')


class CorDosOlhosJob(models.Model):
    job = models.ForeignKey(to='Job', on_delete=models.PROTECT, related_name='cores_dos_olhos')
    cor_dos_olhos = models.ForeignKey(to='promotor.CorDosOlhos', on_delete=models.PROTECT,
                                      related_name='cores_dos_olhos')
    history = HistoricalRecords(table_name='job_cordosolhosjob_history')


class DisponibilidadeJob(models.Model):
    job = models.ForeignKey(to='Job', on_delete=models.PROTECT, related_name='disponibilidades_jobs')
    datahora_inicio = models.DateTimeField()
    datahora_fim = models.DateTimeField()
    history = HistoricalRecords(table_name='job_disponibilidadejob_history')


class Candidatura(models.Model):
    ATIVA = 1
    RETIRADA = 2
    SELECIONADA_ENTREVISTA = 3
    APROVADA = 4
    REPROVADA = 5
    REPROVADA_ENTREVISTA = 6

    STATUS = (
        (ATIVA, 'Ativa'),
        (RETIRADA, 'Retirada'),
        (SELECIONADA_ENTREVISTA, 'Selecionada para entrevista'),
        (APROVADA, 'Aprovada'),
        (REPROVADA, 'Reprovada'),
        (REPROVADA_ENTREVISTA, 'Reprovada na entrevista'),
    )

    promotor = models.ForeignKey(to='promotor.Promotor', on_delete=models.PROTECT, related_name='candidaturas')
    job = models.ForeignKey(to='Job', on_delete=models.PROTECT, related_name='candidaturas')
    status = models.PositiveIntegerField(choices=STATUS, default=1)
    observacao = models.CharField(max_length=255, null=True)
    data_hora_status = models.DateTimeField(auto_now_add=True)
    history = HistoricalRecords(table_name='job_categoria_history')
