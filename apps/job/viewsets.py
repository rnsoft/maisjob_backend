from rest_framework import viewsets, status, generics
from rest_framework.decorators import detail_route
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response

from apps.api.permissions import IsPromotor, IsAdmin, IsCandidaturaPromotorBackend
from .models import Job, Candidatura, TipoJob
from .serializers import JobSerializer, CandidaturaSerializer, TipoJobSerializer


class JobViewSet(viewsets.ModelViewSet):
    queryset = Job.objects.all()
    serializer_class = JobSerializer

    @detail_route(methods=['get'])
    def candidaturas(self, request, pk):
        job = Job.objects.get(id=pk)
        serializer = CandidaturaSerializer(job.candidaturas.all(), many=True)
        return Response(serializer.data)


class JobListViewSet(generics.ListAPIView):
    queryset = Job.objects.all()
    serializer_class = JobSerializer
    permission_classes = (IsPromotor,)


class CandidaturaViewSet(viewsets.ModelViewSet):
    queryset = Candidatura.objects.all()
    serializer_class = CandidaturaSerializer
    permission_classes = (IsPromotor,)
    filter_backends = (IsCandidaturaPromotorBackend,)


class TipoJobViewSet(viewsets.ModelViewSet):
    queryset = TipoJob.objects.all()
    serializer_class = TipoJobSerializer


class CandidaturaStatusViewSet(GenericAPIView):
    def post(self, request, *args, **kwargs):
        candidatura = Candidatura.objects.filter(id=kwargs['candidatura'])
        job = candidatura[0].job
        status_job = kwargs['status']
        if status_job == '4':
            if job.quantidade_vagas_disponiveis > 0:
                job.subtrair_disponnivel()
            else:
                return Response({"error": ['Não existem vagas disponíveis.']}, status=status.HTTP_400_BAD_REQUEST)
        elif (status_job == '5' or status_job == '6') and candidatura[0].status == 4:
            job.adicionar_disponnivel()

        candidatura.update(status=status_job)

        return Response({"sucesso": ['Status alterado com sucesso.']}, status=status.HTTP_201_CREATED)
