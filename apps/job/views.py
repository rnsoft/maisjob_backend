from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, ListView, CreateView, UpdateView, View
from django.contrib.auth.mixins import LoginRequiredMixin

# Create your views here.
from apps.api.permissions import SuperUserRequiredMixin
from .forms import JobForm, TipoJobForm
from .models import Job, TipoJob


class TipoJobListView(SuperUserRequiredMixin, LoginRequiredMixin, ListView):
    model = TipoJob
    template_name = 'cadastros/tipojob/lista.html'
    context_object_name = "tipojob_lista"


class TipoJobCreateView(SuperUserRequiredMixin, LoginRequiredMixin, CreateView):
    model = TipoJob
    template_name = 'cadastros/tipojob/form.html'
    form_class = TipoJobForm

    def get_success_url(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-tipojob-list')

    def get_success_url_novo(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-tipojob-create')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        if 'outro' in self.request.POST:
            return HttpResponseRedirect(self.get_success_url_novo())
        else:
            return HttpResponseRedirect(self.get_success_url())


class TipoJobUpdateView(SuperUserRequiredMixin, LoginRequiredMixin, UpdateView):
    model = TipoJob
    template_name = 'cadastros/tipojob/form.html'
    form_class = TipoJobForm

    def get_success_url(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-tipojob-list')

    def get_success_url_novo(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-tipojob-create')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        if 'outro' in self.request.POST:
            return HttpResponseRedirect(self.get_success_url_novo())
        else:
            return HttpResponseRedirect(self.get_success_url())


class TipoJobDeleteView(SuperUserRequiredMixin, LoginRequiredMixin, View):
    def get(self, request, pk):
        try:
            obj = get_object_or_404(TipoJob, pk=pk)
            obj.delete()
            messages.success(request, 'Excluído(a) com sucesso')
        except:
            messages.error(request, 'Não foi possível excluir')
        return HttpResponseRedirect(reverse('cadastros-tipojob-list'))


class JobListView(LoginRequiredMixin, ListView):
    model = Job
    template_name = 'cadastros/job/lista.html'
    context_object_name = "job_lista"


class JobCreateView(LoginRequiredMixin, CreateView):
    model = Job
    template_name = 'cadastros/job/form.html'
    form_class = JobForm

    def get_success_url(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-job-list')

    def get_success_url_novo(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-job-create')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        if 'outro' in self.request.POST:
            return HttpResponseRedirect(self.get_success_url_novo())
        else:
            return HttpResponseRedirect(self.get_success_url())


class JobUpdateView(LoginRequiredMixin, UpdateView):
    model = Job
    template_name = 'cadastros/job/form.html'
    form_class = JobForm

    def get_success_url(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-job-list')

    def get_success_url_novo(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-job-create')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        if 'outro' in self.request.POST:
            return HttpResponseRedirect(self.get_success_url_novo())
        else:
            return HttpResponseRedirect(self.get_success_url())


class JobDeleteView(LoginRequiredMixin, View):
    def get(self, request, pk):
        try:
            obj = get_object_or_404(Job, pk=pk)
            obj.delete()
            messages.success(request, 'Excluído(a) com sucesso')
        except:
            messages.error(request, 'Não foi possível excluir')
        return HttpResponseRedirect(reverse('cadastros-job-list'))


class CandidaturaView(LoginRequiredMixin, ListView):
    template_name = 'candidatos/lista.html'
    model = Job
    context_object_name = "job_lista"


class CandidaturaJogoView(LoginRequiredMixin, ListView):
    template_name = 'candidatos/jogo.html'
    model = Job
    context_object_name = "job_lista"


