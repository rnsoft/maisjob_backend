from datetime import date
from rest_framework import serializers

from apps.agencia.serializers import AcaoSerializer
from apps.promotor.models import Promotor
from apps.promotor.serializers import PromotorSerializer
from .models import Job, Candidatura, TipoJob, CorDoCabeloJob


class CorDoCabeloJobSerializer(serializers.ModelSerializer):
    class Meta:
        model = CorDoCabeloJob
        fields = ('cor_do_cabelo')


class TipoJobSerializer(serializers.ModelSerializer):
    modo_pagamento_sugerido__display = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = TipoJob
        fields = ('id', 'modo_pagamento_sugerido', 'modo_pagamento_sugerido__display', 'descricao',)

    def get_modo_pagamento_sugerido__display(self, instance):
        return instance.get_modo_pagamento_sugerido_display()


class JobSerializerOnly(serializers.ModelSerializer):
    acao = AcaoSerializer(read_only=True)
    tipo = TipoJobSerializer(read_only=True)

    class Meta:
        model = Job
        fields = ('id', 'acao', 'tipo', 'modo_pagamento', 'quantidade_vagas_totais', 'quantidade_vagas_disponiveis',
                  'quantidade_de_modo_pagamento', 'cache_por_modo_pagamento', 'cache_individual', 'cache_total',
                  'perfil_idade_minima', 'perfil_idade_maxima', 'perfil_altura_minima', 'perfil_altura_maxima',
                  'observacao')


class JobSerializer(serializers.ModelSerializer):
    acao = AcaoSerializer(read_only=True)
    tipo = TipoJobSerializer(read_only=True)
    job_para_mim = serializers.SerializerMethodField(read_only=True)
    candidatura__status = serializers.SerializerMethodField(read_only=True)
    candidatura__id = serializers.SerializerMethodField(read_only=True)
    perfil_cor_cabelo = serializers.SerializerMethodField(read_only=True)
    perfil_sexo = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Job
        fields = ('id', 'acao', 'tipo', 'modo_pagamento', 'quantidade_vagas_totais', 'quantidade_vagas_disponiveis',
                  'quantidade_de_modo_pagamento', 'cache_por_modo_pagamento', 'cache_individual', 'cache_total',
                  'candidatura__status', 'candidatura__id', 'job_para_mim', 'perfil_sexo', 'perfil_idade_minima',
                  'perfil_idade_maxima', 'perfil_altura_minima', 'perfil_altura_maxima', 'perfil_cor_cabelo',
                  'observacao')

    def get_job_para_mim(self, instance):
        promotor = Promotor.objects.get(usuario=self.context['request'].user)
        return promotor.job_para_min(instance)

    def get_candidatura__status(self, instance):
        promotor = Promotor.objects.filter(usuario=self.context['request'].user)
        candidaturas = instance.candidaturas.filter(promotor=promotor, job=instance)

        if candidaturas:
            return candidaturas[0].status

        return None

    def get_candidatura__id(self, instance):
        promotor = Promotor.objects.filter(usuario=self.context['request'].user)
        candidaturas = instance.candidaturas.filter(promotor=promotor, job=instance)

        if candidaturas:
            return candidaturas[0].id

        return None

    def get_perfil_cor_cabelo(self, instance):
        perfil_cor_cabelo = CorDoCabeloJob.objects.filter(job=instance).values_list('cor_do_cabelo__descricao',
                                                                                    flat=True)

        if perfil_cor_cabelo:
            return perfil_cor_cabelo

        return None

    def get_perfil_sexo(self, instance):
        return instance.get_perfil_sexo_display()


class CandidaturaSerializer(serializers.ModelSerializer):
    job = JobSerializerOnly(read_only=True)
    promotor = PromotorSerializer(read_only=True, )
    status__display = serializers.SerializerMethodField(read_only=True)
    idade = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Candidatura
        fields = ('id', 'promotor', 'job', 'idade', 'status', 'observacao', 'data_hora_status', 'status__display')

    def get_status__display(self, instance):
        return instance.get_status_display()

    def get_idade(self, instance):
        today = date.today()
        data_nascimento = instance.promotor.data_nascimento
        idade = today.year - data_nascimento.year - (
            (today.month, today.day) < (data_nascimento.month, data_nascimento.day))
        return idade

    def validate_promotor(self, promotor):
        return promotor

    def validate(self, attrs):
        promotor = self.context['request'].data.get('promotor')
        if promotor:
            try:
                attrs['promotor'] = Promotor.objects.get(id=promotor)
            except Promotor.DoesNotExist as e:
                raise serializers.ValidationError({"promotor": 'Pk inválido "' + promotor + '" - objeto não existe.'})

            job = Job.objects.get(id=self.context['request'].data['job'])
            job_pra_mim = attrs['promotor'].job_para_min(job)

            if not job_pra_mim:
                raise serializers.ValidationError({"promotor": 'Perfil incompatível.'})

            if self.instance:
                if Candidatura.objects.filter(promotor=promotor, job=job,
                                              status=attrs.get('status')):
                    raise serializers.ValidationError({"promotor": 'Você já retirou sua candidatura a job.'})
                if not attrs.get('status') or not attrs.get('status') == 2:
                    raise serializers.ValidationError({"status": 'Status inválido.'})
            else:
                if Candidatura.objects.filter(promotor=promotor, job=job):
                    raise serializers.ValidationError({"promotor": 'Você já é candidato a job.'})
        else:
            raise serializers.ValidationError({"promotor": 'Este campo é obrigatório.'})

        attrs['job'] = job

        return attrs
