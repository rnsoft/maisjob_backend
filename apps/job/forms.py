from django import forms
from django.db import transaction

from apps.promotor.models import CorDaPele, CorDoCabelo, CorDosOlhos
from .models import Job, TipoJob, CorDaPeleJob, CorDoCabeloJob, CorDosOlhosJob


class JobForm(forms.ModelForm):
    cor_de_pele = forms.MultipleChoiceField()
    cor_do_cabelo = forms.MultipleChoiceField()
    cor_dos_olhos = forms.MultipleChoiceField()

    class Meta:
        model = Job
        fields = ('acao', 'tipo', 'modo_pagamento', 'quantidade_vagas_totais', 'quantidade_vagas_disponiveis',
                  'quantidade_de_modo_pagamento', 'cache_por_modo_pagamento', 'cache_individual', 'cache_total',
                  'perfil_sexo', 'perfil_idade_minima', 'perfil_idade_maxima', 'perfil_altura_minima',
                  'perfil_altura_maxima', 'observacao')

    def save(self, commit=True):
        with transaction.atomic():
            cor_de_pele = self.cleaned_data['cor_de_pele']
            cor_do_cabelo = self.cleaned_data['cor_do_cabelo']
            cor_dos_olhos = self.cleaned_data['cor_dos_olhos']

            job = super(JobForm, self).save(commit=True)

            CorDaPeleJob.objects.filter(job=job).delete()
            for cor in cor_de_pele:
                CorDaPeleJob.objects.create(job=job, cor_da_pele_id=cor)

            CorDoCabeloJob.objects.filter(job=job).delete()
            for cor in cor_do_cabelo:
                CorDoCabeloJob.objects.create(job=job, cor_do_cabelo_id=cor)

            CorDosOlhosJob.objects.filter(job=job).delete()
            for cor in cor_dos_olhos:
                CorDosOlhosJob.objects.create(job=job, cor_dos_olhos_id=cor)

            return job

    def __init__(self, *args, **kwargs):
        super(JobForm, self).__init__(*args, **kwargs)
        self.fields['observacao'].required = False
        self.fields['perfil_sexo'].required = False
        self.fields['perfil_idade_minima'].required = False
        self.fields['perfil_idade_maxima'].required = False
        self.fields['perfil_altura_minima'].required = False
        self.fields['perfil_altura_maxima'].required = False

        if self.instance.id:
            self.fields['cor_de_pele'] = forms.MultipleChoiceField(
                choices=[(o.id, str(o)) for o in CorDaPele.objects.all()],
                initial=[(o.cor_da_pele_id) for o in CorDaPeleJob.objects.filter(job_id=self.instance.id)],
                widget=forms.CheckboxSelectMultiple,
                required=False,
            )
            self.fields['cor_do_cabelo'] = forms.MultipleChoiceField(
                choices=[(o.id, str(o)) for o in CorDoCabelo.objects.all()],
                initial=[(o.cor_do_cabelo_id) for o in CorDoCabeloJob.objects.filter(job_id=self.instance.id)],
                widget=forms.CheckboxSelectMultiple,
                required=False,
            )
            self.fields['cor_dos_olhos'] = forms.MultipleChoiceField(
                choices=[(o.id, str(o)) for o in CorDosOlhos.objects.all()],
                initial=[(o.cor_dos_olhos_id) for o in CorDosOlhosJob.objects.filter(job_id=self.instance.id)],
                widget=forms.CheckboxSelectMultiple,
                required=False,
            )
        else:
            self.fields['cor_de_pele'] = forms.MultipleChoiceField(
                choices=[(o.id, str(o)) for o in CorDaPele.objects.all()],
                widget=forms.CheckboxSelectMultiple,
                required=False,
            )
            self.fields['cor_do_cabelo'] = forms.MultipleChoiceField(
                choices=[(o.id, str(o)) for o in CorDoCabelo.objects.all()],
                widget=forms.CheckboxSelectMultiple,
                required=False,
            )
            self.fields['cor_dos_olhos'] = forms.MultipleChoiceField(
                choices=[(o.id, str(o)) for o in CorDosOlhos.objects.all()],
                widget=forms.CheckboxSelectMultiple,
                required=False,
            )

    def clean_quantidade_vagas_disponiveis(self):
        data = self.data

        if int(data.get('quantidade_vagas_disponiveis')) > int(data.get('quantidade_vagas_totais')):
            raise forms.ValidationError('A quantidade de vagas disponíveis deve ser menor ou igual à quantidade total.')

        return data.get('quantidade_vagas_disponiveis')


class TipoJobForm(forms.ModelForm):
    class Meta:
        model = TipoJob
        fields = ('descricao', 'modo_pagamento_sugerido',)
