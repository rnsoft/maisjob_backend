from django.db.models import Count, Q
from django.db.models.functions import Coalesce
from django.shortcuts import render

# Create your views here.
from django.core.paginator import Paginator
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.views.generic import TemplateView, ListView, CreateView, UpdateView, View
from django.contrib.auth.mixins import LoginRequiredMixin

from apps.api.permissions import SuperUserRequiredMixin
from .models import Acao, Agencia
from .forms import AcaoForm, AgenciaForm


class AcaoListView(LoginRequiredMixin, ListView):
    model = Acao
    template_name = 'cadastros/acao/lista.html'
    context_object_name = "acao_lista"


class AcaoCreateView(LoginRequiredMixin, CreateView):
    model = Acao
    template_name = 'cadastros/acao/form.html'
    form_class = AcaoForm

    def get_success_url(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-acao-list')

    def get_success_url_novo(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-acao-create')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        if 'outro' in self.request.POST:
            return HttpResponseRedirect(self.get_success_url_novo())
        else:
            return HttpResponseRedirect(self.get_success_url())


class AcaoUpdateView(LoginRequiredMixin, UpdateView):
    model = Acao
    template_name = 'cadastros/acao/form.html'
    form_class = AcaoForm

    def get_success_url(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-acao-list')

    def get_success_url_novo(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-acao-create')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        if 'outro' in self.request.POST:
            return HttpResponseRedirect(self.get_success_url_novo())
        else:
            return HttpResponseRedirect(self.get_success_url())


class AcaoDeleteView(LoginRequiredMixin, View):
    def get(self, request, pk):
        try:
            obj = get_object_or_404(Acao, pk=pk)
            obj.delete()
            messages.success(request, 'Excluído(a) com sucesso')
        except:
            messages.error(request, 'Não foi possível excluir')
        return HttpResponseRedirect(reverse('cadastros-acao-list'))


class AgenciaListView(SuperUserRequiredMixin, LoginRequiredMixin, ListView):
    model = Agencia
    template_name = 'cadastros/agencia/lista.html'
    context_object_name = "agencia_lista"


class AgenciaCreateView(SuperUserRequiredMixin, LoginRequiredMixin, CreateView):
    model = Agencia
    template_name = 'cadastros/agencia/form.html'
    form_class = AgenciaForm

    def get_success_url(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-agencia-list')

    def get_success_url_novo(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-agencia-create')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        if 'outro' in self.request.POST:
            return HttpResponseRedirect(self.get_success_url_novo())
        else:
            return HttpResponseRedirect(self.get_success_url())


class AgenciaUpdateView(SuperUserRequiredMixin, LoginRequiredMixin, UpdateView):
    model = Agencia
    template_name = 'cadastros/agencia/form.html'
    form_class = AgenciaForm

    def get_success_url(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-agencia-list')

    def get_success_url_novo(self):
        messages.success(self.request, 'Salvo com sucesso')
        return reverse('cadastros-agencia-create')

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        if 'outro' in self.request.POST:
            return HttpResponseRedirect(self.get_success_url_novo())
        else:
            return HttpResponseRedirect(self.get_success_url())


class AgenciaDeleteView(SuperUserRequiredMixin, LoginRequiredMixin, View):
    def get(self, request, pk):
        try:
            obj = get_object_or_404(Agencia, pk=pk)
            obj.delete()
            messages.success(request, 'Excluído(a) com sucesso')
        except:
            messages.error(request, 'Não foi possível excluir')
        return HttpResponseRedirect(reverse('cadastros-agencia-list'))
