from rest_framework import serializers

from apps.core.serializers import CidadeSerializer
from apps.promotor.serializers import ImagemSerializer
from .models import Agencia, Acao


class AgenciaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Agencia
        fields = ('id', 'cpf_cnpj', 'tipo_pessoa', 'nome_exibicao', 'nome_razao_social',
                  'contato', 'telefone', 'email', 'datahora_inclusao', 'datahora_alteracao',)


class AcaoSerializer(serializers.ModelSerializer):
    agencia = AgenciaSerializer(read_only=True)
    cidade = CidadeSerializer(read_only=True)
    imagens = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Acao
        fields = ('id', 'agencia', 'cidade', 'datahora_inicio_exibicao', 'datahora_fim_exibicao', 'datahora_inicio_acao',
                  'datahora_fim_acao', 'publico_titulo', 'publico_descricao', 'publico_local',
                  'publico_tem_entrevista', 'publico_local_entrevista', 'publico_data_hora_entrevista', 'imagens')

    def get_imagens(self, instance):
        return ImagemSerializer(instance.imagens.all(), many=True).data
