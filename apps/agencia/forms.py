from datetime import datetime

from django import forms

from apps.core.models import Cidade
from .models import Acao, Agencia


class AcaoForm(forms.ModelForm):
    class Meta:
        model = Acao
        fields = ('agencia', 'cidade', 'datahora_inicio_exibicao', 'datahora_fim_exibicao', 'datahora_inicio_acao',
                  'datahora_fim_acao', 'publico_titulo', 'publico_descricao', 'publico_local', 'publico_tem_entrevista',
                  'publico_local_entrevista', 'publico_data_hora_entrevista',)

    def __init__(self, *args, **kwargs):
        super(AcaoForm, self).__init__(*args, **kwargs)
        self.fields['publico_data_hora_entrevista'].required = False
        self.fields['publico_local_entrevista'].required = False
        self.fields['publico_descricao'].required = False

    def clean_datahora_inicio_acao(self):
        data = self.data

        inicio = data.get('datahora_inicio_acao')
        fim_exibicao = data.get('datahora_fim_exibicao')

        if len(inicio) == 19:
            inicio = datetime.strptime(inicio, '%d/%m/%Y  %H:%M:%S')
        else:
            inicio = datetime.strptime(inicio, '%d/%m/%Y  %H:%M')

        if len(fim_exibicao) == 19:
            fim_exibicao = datetime.strptime(fim_exibicao, '%d/%m/%Y  %H:%M:%S')
        else:
            fim_exibicao = datetime.strptime(fim_exibicao, '%d/%m/%Y  %H:%M')

        fim = data.get('datahora_fim_acao')

        if len(fim) == 19:
            fim = datetime.strptime(fim, '%d/%m/%Y  %H:%M:%S')
        else:
            fim = datetime.strptime(fim, '%d/%m/%Y  %H:%M')

        if inicio < fim_exibicao:
            frase = 'A data e hora do início da AÇÃO deve ser POSTERIOR à data e hora do fim da exibição.'
            raise forms.ValidationError(frase)

        return inicio

    def clean_datahora_fim_exibicao(self):
        data = self.data

        inicio = data.get('datahora_inicio_exibicao')

        if len(inicio) == 19:
            inicio = datetime.strptime(inicio, '%d/%m/%Y  %H:%M:%S')
        else:
            inicio = datetime.strptime(inicio, '%d/%m/%Y  %H:%M')

        fim = data.get('datahora_fim_exibicao')

        if len(fim) == 19:
            fim = datetime.strptime(fim, '%d/%m/%Y  %H:%M:%S')
        else:
            fim = datetime.strptime(fim, '%d/%m/%Y  %H:%M')

        if inicio > fim:
            frase = 'A data e hora do fim da exibição devem ser POSTERIORES a data e hora do início da exibição.'
            raise forms.ValidationError(frase)

        return inicio

    def clean_datahora_inicio_exibicao(self):
        data = self.data

        inicio = datetime.now()
        fim = data.get('datahora_inicio_exibicao')

        if len(fim) == 19:
            fim = datetime.strptime(fim, '%d/%m/%Y  %H:%M:%S')
        else:
            fim = datetime.strptime(fim, '%d/%m/%Y  %H:%M')

        if inicio > fim:
            frase = 'A data e hora do início da exibição deve ser POSTERIOR à data e hora ATUAIS.'
            raise forms.ValidationError(frase)

        return inicio

    def clean_publico_data_hora_entrevista(self):
        data = self.data

        if data.get('publico_tem_entrevista') and not data.get('publico_data_hora_entrevista'):
            raise forms.ValidationError('Esse campo é obrigatório quando tem entrevista.')

        inicio = data.get('publico_data_hora_entrevista')

        if inicio:
            fim_exibicao = data.get('datahora_fim_exibicao')

            if inicio < fim_exibicao:
                frase = 'A data e hora da entrevista deve ser POSTERIOR à data e hora do fim da exibição.'
                raise forms.ValidationError(frase)

        return data.get('publico_data_hora_entrevista')


class AgenciaForm(forms.ModelForm):
    class Meta:
        model = Agencia
        fields = ('cpf_cnpj', 'tipo_pessoa', 'nome_exibicao', 'nome_razao_social', 'contato',
                  'telefone', 'email')