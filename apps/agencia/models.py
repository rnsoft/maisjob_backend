from django.db import models

# Create your models here.
from simple_history.models import HistoricalRecords
from hashids import Hashids
from random import randint


class Agencia(models.Model):
    FISICA = 1
    JURIDICA = 2

    PESSOA = (
        (FISICA, 'Pessoa física'),
        (JURIDICA, 'Pessoa jurídica')
    )
    codigo_indicacao = models.CharField(max_length=8, unique=True, null=True)
    cpf_cnpj = models.CharField(max_length=18, unique=True)
    tipo_pessoa = models.PositiveIntegerField(choices=PESSOA)
    nome_exibicao = models.CharField(max_length=255)
    nome_razao_social = models.CharField(max_length=255)
    contato = models.CharField(max_length=255)
    telefone = models.CharField(max_length=255)
    email = models.EmailField()
    datahora_inclusao = models.DateTimeField(auto_now_add=True)
    datahora_alteracao = models.DateTimeField(auto_now=True)

    history = HistoricalRecords(table_name='agencia_agencia_history')

    def __str__(self):
        return self.nome_exibicao

    def save(self, *args, **kwargs):
        if self.id:
            return super(Agencia, self).save(*args, **kwargs)
        try:
            new_numero = Agencia.objects.latest('id').id + 1
        except Agencia.DoesNotExist:
            new_numero = 1
        self.codigo_indicacao = (Hashids(salt='nova_agencia', min_length=8).encrypt(new_numero + randint(0, 9999))).lower()
        return super(Agencia, self).save(*args, **kwargs)


class Acao(models.Model):
    agencia = models.ForeignKey(to='Agencia', on_delete=models.PROTECT, related_name='acoes')
    cidade = models.ForeignKey(to='core.Cidade', on_delete=models.PROTECT, related_name='acoes', null=True)
    datahora_inicio_exibicao = models.DateTimeField()
    datahora_fim_exibicao = models.DateTimeField()
    datahora_inicio_acao = models.DateTimeField()
    datahora_fim_acao = models.DateTimeField()
    publico_titulo = models.CharField(max_length=255)
    publico_descricao = models.CharField(max_length=255, null=True)
    publico_local = models.CharField(max_length=255, null=True)
    publico_tem_entrevista = models.BooleanField(default=False)
    publico_local_entrevista = models.CharField(max_length=255, null=True)
    publico_data_hora_entrevista = models.CharField(max_length=255, null=True)

    history = HistoricalRecords(table_name='agencia_acao_history')

    def __str__(self):
        return self.publico_titulo


class Cliente(models.Model):
    FISICA = 1
    JURIDICA = 2

    PESSOA = (
        (FISICA, 'Pessoa física'),
        (JURIDICA, 'Pessoa jurídica')
    )

    agencia = models.ForeignKey(to='Agencia', on_delete=models.PROTECT, related_name='clientes')
    cpf_cnpj = models.CharField(max_length=18, unique=True)
    tipo_pessoa = models.PositiveIntegerField(choices=PESSOA)
    nome_exibicao = models.CharField(max_length=255)
    nome_razao_social = models.CharField(max_length=255)
    contato = models.CharField(max_length=255)
    telefone = models.CharField(max_length=255)
    email = models.EmailField()

    history = HistoricalRecords(table_name='agencia_cliente_history')
