from rest_framework import viewsets, status, generics
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from apps.api.permissions import IsPromotor, IsPromotorBackend
from apps.job.serializers import JobSerializer
from .models import Agencia
from .serializers import AgenciaSerializer


class AgenciaListViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Agencia.objects.all()
    serializer_class = AgenciaSerializer
    permission_classes = (IsPromotor,)

    @detail_route(methods=['get'])
    def jobs(self, request, pk):
        agencia = Agencia.objects.get(id=pk)
        acoes = agencia.acoes.all()

        jobs = []

        for acao in acoes:
            x = acao.jobs.all()
            for z in x:
                jobs.append(z)

        serializer = JobSerializer(jobs, many=True)
        return Response(serializer.data)
