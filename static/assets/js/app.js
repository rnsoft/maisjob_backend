var UTILS = function () {
    return {
        init: function () {
            $(document).on('click', ':not(form)[data-confirm]', function () {
                return confirm($(this).data('confirm'));
            });

            $(document).on('submit', 'form[data-confirm]', function () {
                return confirm($(this).data('confirm'));
            });

            $(document).on('input', 'select', function (event) {
                var msg = $(this).children('option:selected').data('confirm');
                if (msg != undefined && !confirm(msg)) {
                    $(this)[0].selectedIndex = 0;
                }
            });

            jQuery.datetimepicker.setLocale('pt-BR');

            $("input[rel='time']").inputmask("99:99");

            $("input[rel='cpf']").inputmask("999.999.999-99");

            $("input[rel='date']").inputmask("99/99/9999").datetimepicker({
                timepicker: false,
                format: 'd/m/Y'
            });
        }
    }
}();

var API_URL = $("body").data('base-url');

jQuery(document).ready(function () {
    UTILS.init();

    $('.toolip').popup();
});

Chart.pluginService.register({
    beforeInit: function (chart) {

        if (chart.config.type !== 'horizontalBar') {
            var hasWrappedTicks = chart.config.data.labels.some(function (label) {
                return label.indexOf('\n') !== -1;
            });

            if (hasWrappedTicks) {
                // figure out how many lines we need - use fontsize as the height of one line
                var tickFontSize = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].ticks.fontSize, Chart.defaults.global.defaultFontSize);
                var maxLines = chart.config.data.labels.reduce(function (maxLines, label) {
                    return Math.max(maxLines, label.split('\n').length);
                }, 0);
                var height = (tickFontSize + 2) * maxLines + (chart.options.scales.xAxes[0].ticks.padding || 0);

                // insert a dummy box at the bottom - to reserve space for the labels
                Chart.layoutService.addBox(chart, {
                    draw: Chart.helpers.noop,
                    isHorizontal: function () {
                        return true;
                    },
                    update: function () {
                        return {
                            height: this.height
                        };
                    },
                    height: height,
                    options: {
                        position: 'bottom',
                        fullWidth: 1,
                    }
                });

                // turn off x axis ticks since we are managing it ourselves
                chart.options = Chart.helpers.configMerge(chart.options, {
                    scales: {
                        xAxes: [{
                            ticks: {
                                display: false,
                                // set the fontSize to 0 so that extra labels are not forced on the right side
                                fontSize: 0
                            }
                        }]
                    }
                });

                chart.hasWrappedTicks = {
                    tickFontSize: tickFontSize
                };
            }
        }
    },
    afterDraw: function (chart) {
        if (chart.hasWrappedTicks && chart.config.type !== 'horizontalBar') {
            // draw the labels and we are done!
            chart.chart.ctx.save();
            var tickFontSize = chart.hasWrappedTicks.tickFontSize;
            var tickFontStyle = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].ticks.fontStyle, Chart.defaults.global.defaultFontStyle);
            var tickFontFamily = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].ticks.fontFamily, Chart.defaults.global.defaultFontFamily);
            var tickLabelFont = Chart.helpers.fontString(tickFontSize, tickFontStyle, tickFontFamily);
            chart.chart.ctx.font = tickLabelFont;
            chart.chart.ctx.textAlign = 'center';
            var tickFontColor = Chart.helpers.getValueOrDefault(chart.options.scales.xAxes[0].fontColor, Chart.defaults.global.defaultFontColor);
            chart.chart.ctx.fillStyle = tickFontColor;

            var meta = chart.getDatasetMeta(0);
            var xScale = chart.scales[meta.xAxisID];
            var yScale = chart.scales[meta.yAxisID];

            chart.config.data.labels.forEach(function (label, i) {
                label.split('\n').forEach(function (line, j) {
                    chart.chart.ctx.fillText(line, xScale.getPixelForTick(i + 0.5), (chart.options.scales.xAxes[0].ticks.padding || 0) + yScale.getPixelForValue(yScale.min) +
                        // move j lines down
                        j * (chart.hasWrappedTicks.tickFontSize + 2));
                });
            });

            chart.chart.ctx.restore();
        }
    }
});